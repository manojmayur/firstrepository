﻿using RestaurantManagementSystem.CustomExceptions;
using RestaurantManagementSystem.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagementSystem.DAL
{
    public class BillDAL
    {
        private readonly DatabaseConnection connection = new DatabaseConnection();

        public int AddBill(Bill bill,Staff staff)
        {
            int rowsAffected = 0;
            try
            {
                connection.OpenConnection();

                SqlCommand command = new SqlCommand("AddBill", connection.GetConnection());
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter parameter;

                parameter = command.Parameters.Add("@Billdate", SqlDbType.Date);
                parameter.Value = bill.BillDate;

                parameter = command.Parameters.Add("@StaffId", SqlDbType.Int);
                parameter.Value = staff.StaffId;

                parameter = command.Parameters.Add("@DeliveryMode", SqlDbType.VarChar, 20);
                parameter.Value = bill.DeliveryMode;

                parameter = command.Parameters.Add("@Amount", SqlDbType.Float);
                parameter.Value = bill.Amount;

                rowsAffected = command.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Data not inserted", ex);
            }
            finally
            {
                connection.CloseConnection();
            }
            return rowsAffected;
        }
        public List<Bill> GetBillDetails()
        {
           
            List<Bill> bills = new List<Bill>();
            try
            {
                connection.OpenConnection();
                SqlCommand command = new SqlCommand("GetBillDetails", connection.GetConnection());
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader details = command.ExecuteReader();
                while (details.Read())
                {
                    Bill bill = new Bill()
                    {
                        BillNumber = details.GetInt32(0),
                        BillDate = (DateTime)details.GetValue(1),
                        StaffId = details.GetInt32(2),
                        DeliveryMode = details.GetString(3),
                        Amount = details.GetDecimal(4)
                    };
                    bills.Add(bill);
                }
                details.Close();
            }
            catch (SqlException ex)
            {
                throw new NoDataFoundException("Data not found", ex);
            }
            finally
            {
                connection.CloseConnection();
            }
            return bills;
        }
        public List<Bill> GetBillsByDate(int year,int month,int day)
        {
            List<Bill> bills = new List<Bill>();
            
                connection.OpenConnection();
                SqlCommand command = new SqlCommand("DisplayBillsByDate", connection.GetConnection());
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@day", day);
                command.Parameters.AddWithValue("@month", month);
                command.Parameters.AddWithValue("@year", year);

                var details = command.ExecuteReader();
                while (details.Read())
                {
                    Bill bill = new Bill()
                    {
                        BillNumber = details.GetInt32(0),
                        BillDate = details.GetDateTime(1),
                        StaffId = details.GetInt32(2),
                        DeliveryMode = details.GetString(3),
                        Amount = details.GetDecimal(4)
                    };
                    bills.Add(bill);
                }
                details.Close();
                connection.CloseConnection();
                return bills;
        }
    }
}
