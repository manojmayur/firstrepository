﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace RestaurantManagementSystem.DAL
{
    class DatabaseConnection
    {
        private static readonly string _connectionString = ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;
        private readonly SqlConnection connection = new SqlConnection(_connectionString);

        public SqlConnection GetConnection()
        {
            return connection;
        }
        public void OpenConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void CloseConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }
}
