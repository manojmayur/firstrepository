﻿using RestaurantManagementSystem.CustomExceptions;
using RestaurantManagementSystem.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagementSystem.DAL
{
    public class StaffDAL
    {
        private readonly DatabaseConnection connection = new DatabaseConnection();

        public int AddStaff(Staff staff)
        {
            int rowsAffected = 0;
            try
            {
                connection.OpenConnection();

                SqlCommand command = new SqlCommand("AddStaffDetails", connection.GetConnection());
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter parameter;

                parameter = command.Parameters.Add("@StaffName", SqlDbType.VarChar, 50);
                parameter.Value = staff.StaffName;

                parameter = command.Parameters.Add("@Gender", SqlDbType.VarChar, 20);
                parameter.Value = staff.Gender;

                parameter = command.Parameters.Add("@ShiftType", SqlDbType.VarChar, 20);
                parameter.Value = staff.Shift;

                rowsAffected = command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Data not inserted", ex);
            }
            finally
            {
                connection.CloseConnection();
            }
            return rowsAffected;
        }
        public List<Staff> GetStaffDetails()
        {
            List<Staff> staffs = new List<Staff>();
            try
            {
                connection.OpenConnection();

                SqlCommand command = new SqlCommand("GetStaffDetails", connection.GetConnection());
                command.CommandType = System.Data.CommandType.StoredProcedure;
                var details = command.ExecuteReader();
                while (details.Read())
                {
                    Staff staff = new Staff()
                    {
                        StaffId = details.GetInt32(0),
                        StaffName = details.GetString(1),
                        Gender = details.GetString(2),
                        Shift = details.GetString(3)
                    };
                    staffs.Add(staff);
                }
                details.Close();
            }
            catch (SqlException ex)
            {
                throw new NoDataFoundException("No details found", ex);
            }
            finally
            {
                connection.CloseConnection();
            }
            return staffs;
        }
    }
}
