﻿using RestaurantManagementSystem.DAL;
using RestaurantManagementSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagementSystem.BL
{
    public class BillBL
    {
        BillDAL billDAL = new BillDAL();

        public int AddBill(Bill bill, Staff staff)
        {
            return billDAL.AddBill(bill, staff);
        }
        public List<Bill> GetBillDetails()
        {
            return billDAL.GetBillDetails();
        }
        public List<Bill> GetBillsByDate(int year,int month,int day)
        {
            return billDAL.GetBillsByDate(year, month, day);
        }
    }
}
