﻿using RestaurantManagementSystem.DAL;
using RestaurantManagementSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagementSystem.BL
{
    public class StaffBL
    {
        StaffDAL staffDAL = new StaffDAL();
        public int AddStaff(Staff staff)
        {
            return staffDAL.AddStaff(staff);
        }
        public List<Staff> GetStaffDetails()
        {
            return staffDAL.GetStaffDetails();
        }
    }
}
