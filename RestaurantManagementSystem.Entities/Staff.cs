﻿namespace RestaurantManagementSystem.Entities
{
    public class Staff
    {
        public int StaffId
        {
            get; set;
        }
        public string StaffName
        {
            get; set;
        }
        public string Gender
        {
            get; set;
        }
        public string Shift
        {
            get; set;
        }
    }
}
