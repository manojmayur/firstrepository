﻿using System;

namespace RestaurantManagementSystem.Entities
{
    public class Bill
    {
        public int BillNumber
        {
            get; set;
        }
        public DateTime BillDate
        {
            get; set;
        }
        public int StaffId
        {
            get; set;
        }
        public string DeliveryMode
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        }
    }
}
