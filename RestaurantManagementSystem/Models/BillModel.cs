﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RestaurantManagementSystem.Models
{
    public class BillModel
    {
        public int BillNumber
        {
            get; set;
        }
        [DataType(DataType.Date,ErrorMessage ="Invalid Date")]
        public DateTime BillDate
        {
            get; set;
        }
        [Required(ErrorMessage ="Please choose the staff")]
        public int StaffId
        {
            get; set;
        }
        [Required(ErrorMessage ="Please Select the Delivery mode")]
        public string DeliveryMode
        {
            get; set;
        }
        [Required]
        [Range(1,int.MaxValue,ErrorMessage ="Please Enter the Valid Amount")]
        public decimal Amount
        {
            get; set;
        }
    }
}