﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RestaurantManagementSystem.Models
{
    public class StaffModel
    {
        public int StaffId
        {
            get; set;
        }
        [Required(ErrorMessage ="Please Enter your name")]
        public string StaffName
        {
            get; set;
        }
        [Required(ErrorMessage ="Please choose your gender")]
        public string Gender
        {
            get; set;
        }
        [Required(ErrorMessage ="Please choose the shift")]
        public string Shift
        {
            get; set;
        }
    }
}