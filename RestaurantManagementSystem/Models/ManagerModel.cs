﻿using RestaurantManagementSystem.BL;
using RestaurantManagementSystem.Entities;
using System;
using System.Collections.Generic;

namespace RestaurantManagementSystem.Models
{
    public class ManagerModel
    {
        StaffBL staffBL = new StaffBL();
        BillBL billBL = new BillBL();
        public Staff ChangeStaffModelToEntity(StaffModel staffModel)
        {
            Staff staff = new Staff()
            {
                StaffId = staffModel.StaffId,
                StaffName = staffModel.StaffName,
                Gender = staffModel.Gender,
                Shift = staffModel.Shift
            };
            return staff;
        }
        public StaffModel ChangeStaffEntityToModel(Staff staff)
        {
            StaffModel staffModel = new StaffModel()
            {
                StaffId = staff.StaffId,
                StaffName = staff.StaffName,
                Gender = staff.Gender,
                Shift = staff.Shift
            };
            return staffModel;
        }
        public Bill ChangeBillModelToEntity(BillModel billModel)
        {
            Bill bill = new Bill()
            {
                BillNumber = billModel.BillNumber,
                BillDate = billModel.BillDate,
                StaffId = billModel.StaffId,
                DeliveryMode = billModel.DeliveryMode,
                Amount = billModel.Amount
            };
            return bill;
        }
        public BillModel ChangeBillEntityToModel(Bill bill)
        {
            BillModel billModel = new BillModel()
            {
                BillNumber = bill.BillNumber,
                BillDate = bill.BillDate,
                StaffId = bill.StaffId,
                DeliveryMode = bill.DeliveryMode,
                Amount = bill.Amount
            };
            return billModel;
        }
        public int AddStaff(StaffModel staffModel)
        {
            return staffBL.AddStaff(ChangeStaffModelToEntity(staffModel));
        }
        public List<StaffModel> GetStaffDetails()
        {
            List<StaffModel> staffModels = new List<StaffModel>();
            List<Staff> staffs = staffBL.GetStaffDetails();
            foreach (var staff in staffs)
            {
                staffModels.Add(ChangeStaffEntityToModel(staff));
            }
            return staffModels;
        }
        public int AddBill(BillModel billModel, StaffModel staffModel)
        {
            return billBL.AddBill(ChangeBillModelToEntity(billModel), ChangeStaffModelToEntity(staffModel));
        }
        public List<BillModel> GetBillDetails()
        {
            List<BillModel> billModels = new List<BillModel>();
            List<Bill> bills = billBL.GetBillDetails();
            foreach (var bill in bills)
            {
                billModels.Add(ChangeBillEntityToModel(bill));
            }
            return billModels;
        }
        public List<BillModel> GetBillsByDate(int year, int month, int day)
        {
            List<BillModel> billModels = new List<BillModel>();
            List<Bill> bills = billBL.GetBillsByDate(year, month, day);
            foreach (var bill in bills)
            {
                billModels.Add(ChangeBillEntityToModel(bill));
            }
            return billModels;
        }
        public double CalculateTotal(List<BillModel> billModels)
        {
            double total = 0;
            foreach (var bill in billModels)
            {
                total += (double)bill.Amount;
            }
            return total;
        }
    }
}