﻿using RestaurantManagementSystem.CustomExceptions;
using RestaurantManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RestaurantManagementSystem.Controllers
{
    public class BillController : Controller
    {
        ManagerModel managerModel = new ManagerModel();
        // GET: Bill
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult AddBill()
        {
            ViewBag.Data = managerModel.GetStaffDetails();
            return View();
        }
        [HttpPost]
        public ActionResult AddBill(BillModel billModel, StaffModel staffModel)
        {
            try
            {
                billModel.BillDate = DateTime.Now;
                int rowsAffected = managerModel.AddBill(billModel, staffModel);
                ViewBag.Data = managerModel.GetStaffDetails();
                if (ViewBag.Data != null && rowsAffected > 0)
                {
                    ViewBag.Message = "Bill generated Succesfully";
                }
                else
                {
                    ViewBag.Message = "Unable to generate bill";
                }
            }
            catch (CustomSqlException ex)
            {
                ViewBag.Message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View();
        }
        [HttpGet]
        public ActionResult GetBillDetails()
        {
            try
            {
                var billModels = managerModel.GetBillDetails();
                return View(billModels);
            }
            catch (NoDataFoundException ex)
            {
                ViewBag.Message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View("Index","Home");
        }
        [HttpGet]
        public ActionResult GetBillsByDate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetBillsByDate(string billDate)
        {
            string[] dates = billDate.Split('-');
            int year = int.Parse(dates[0]);
            int month = int.Parse(dates[1]);
            int day = int.Parse(dates[2]);

            List<BillModel> bills = managerModel.GetBillsByDate(year,month,day);
            ViewBag.Total ="Total Amount :"+managerModel.CalculateTotal(bills);
            ViewBag.Bills = bills;

            return View();
        }
    }
}