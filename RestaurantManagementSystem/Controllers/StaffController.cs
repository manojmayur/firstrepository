﻿using RestaurantManagementSystem.CustomExceptions;
using RestaurantManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RestaurantManagementSystem.Controllers
{
    public class StaffController : Controller
    {
        readonly ManagerModel managerModel = new ManagerModel();
        // GET: Staff
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult AddStaff()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddStaff(StaffModel staffModel)
        {
            try
            {
                int rowsAffected = managerModel.AddStaff(staffModel);
                if (rowsAffected > 0)
                {
                    ViewBag.Message = "Staff added succesfully";
                }
                else
                {
                    ViewBag.Message = "Unable to add staff";
                }
            }
            catch (CustomSqlException ex)
            {
                ViewBag.Message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View();
        }
        [HttpGet]
        public ActionResult GetStaffDetails()
        {
            try
            {
                List<StaffModel> staffs = new List<StaffModel>();
                staffs = managerModel.GetStaffDetails();
                return View(staffs);
            }
            catch (NoDataFoundException ex)
            {
                ViewBag.Message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View("Index", "Home");
        }
    }
}