﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagementSystem.CustomExceptions
{
    public class CustomSqlException : Exception
    {
        public CustomSqlException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
