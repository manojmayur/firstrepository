﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagementSystem.CustomExceptions
{
    public class NoDataFoundException : Exception
    {
        public NoDataFoundException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
